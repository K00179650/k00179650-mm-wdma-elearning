﻿package states
{ 
	import Game;
	import interfaces.IState;
	import objects.Background;
	import objects.Title;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.BeginButton;
	import objects.InstructionButton;
	import objects.CreditButton;
	import objects.CountingWithOliver;
	import objects.SpeechBubbleMenu;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class Menu extends Sprite implements IState
	{
		public var game:Game;
		public var background:Background;
	    public var title:Title;
		public var countingWithOliver: CountingWithOliver;
		public var beginButton: BeginButton;
		public var instructionButton: InstructionButton;
		public var creditButton: CreditButton;
		public var speechBubbleMenu: SpeechBubbleMenu;
		public var clock:Timer;
		
		public function Menu(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			
			background = new Background();
			addChild(background);

			countingWithOliver = new CountingWithOliver();
			addChild(countingWithOliver);
			countingWithOliver.x = 150;
			countingWithOliver.y = 25;
			countingWithOliver.width = 350;
			countingWithOliver.height = 250;
			
			beginButton = new BeginButton();
			addChild(beginButton);
			beginButton.x = 245;
			beginButton.y = 325;
			beginButton.width = 175;
			beginButton.height = 125;
			beginButton.addEventListener(MouseEvent.CLICK, onPlay); 
			beginButton.addEventListener(MouseEvent.CLICK, onClickSound); 
			
			instructionButton = new InstructionButton();
			addChild(instructionButton);
			instructionButton.x = 245;
			instructionButton.y = 525;
			instructionButton.width = 175;
			instructionButton.height = 125;
			instructionButton.addEventListener(MouseEvent.CLICK, onInstructions); 
			instructionButton.addEventListener(MouseEvent.CLICK, onClickSound); 
			
			creditButton = new CreditButton();
			addChild(creditButton);
			creditButton.x = 245;
			creditButton.y = 725;
			creditButton.width = 175;
			creditButton.height = 125;
			creditButton.addEventListener(MouseEvent.CLICK, onCredit); 
			creditButton.addEventListener(MouseEvent.CLICK, onClickSound); 
			
			speechBubbleMenu = new SpeechBubbleMenu();
			addChild(speechBubbleMenu);
			speechBubbleMenu.x = 80;
			speechBubbleMenu.y = 620;
			speechBubbleMenu.width = 375;
			speechBubbleMenu.height = 175;
			speechBubbleMenu.addEventListener(MouseEvent.CLICK, onSpeechBubble); 
			
			clock = new Timer(8000, 1);
			clock.addEventListener(TimerEvent.TIMER, onSpeechBubble)
			clock.start();
		}
		
		private function onSpeechBubble(event:Event):void
		{
		
		clock.stop();
		removeChild(speechBubbleMenu);
	
		}
		
		private function onClickSound(event:MouseEvent):void
		{
			
		var clickonSound:ClickOn = new ClickOn();
			
		var clickonSoundChannel:SoundChannel = clickonSound.play()
			
		}
		
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onInstructions(event:Event):void
		{
			game.changeState(Game.INSTRUCTIONS_STATE);
		}
		
		private function onCredit(event:Event):void
		{
			game.changeState(Game.CREDIT_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			background.removeFromParent();
			background = null;
			
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}