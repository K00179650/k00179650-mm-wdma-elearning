﻿package states
{ 
	import Game;
	import interfaces.IState;
	import objects.Background;
	import objects.Title;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.GameOverTitle;
	import objects.BeginButton;
	import objects.BackButton;
	import objects.ScoreGameOver;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class GameOver extends Sprite implements IState
	{
		public var game:Game;
		public var background:Background;
		public var gameOverTitle: GameOverTitle;
		public var beginButton:BeginButton;
		public var backButton:BackButton;
		public var speechBubbleOver: SpeechBubbleOver;
		public var clock:Timer;
		public var scoreGameOver:ScoreGameOver;
		
		public function GameOver(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);

			scoreGameOver = new ScoreGameOver();
			addChild(scoreGameOver);
			scoreGameOver.x = 500;
			scoreGameOver.y = 8;
			scoreGameOver.width = 100;
			scoreGameOver.height = 75;
			
			gameOverTitle = new GameOverTitle();
			addChild(gameOverTitle);
			gameOverTitle.x = 175;
			gameOverTitle.y = 68;
			gameOverTitle.width = 300;
			gameOverTitle.height = 150;

			beginButton = new BeginButton();
			addChild(beginButton);
			beginButton.x = 245;
			beginButton.y = 325;
			beginButton.width = 175;
			beginButton.height = 125;
			beginButton.addEventListener(MouseEvent.CLICK, onPlay);
			
			backButton = new BackButton();
			addChild(backButton);
			backButton.x = 245;
			backButton.y = 575;
			backButton.width = 175;
			backButton.height = 125;
			backButton.addEventListener(MouseEvent.CLICK, onMenu); 			

			speechBubbleOver = new SpeechBubbleOver();
			addChild(speechBubbleOver);
			speechBubbleOver.x = 80;
			speechBubbleOver.y = 620;
			speechBubbleOver.width = 375;
			speechBubbleOver.height = 175;
			speechBubbleOver.addEventListener(MouseEvent.CLICK, onSpeechBubble); 
			
			clock = new Timer(8000, 1);
			clock.addEventListener(TimerEvent.TIMER, onSpeechBubble)
			clock.start();
		}

		private function onSpeechBubble(event:Event):void
		{
		
		clock.stop();
		removeChild(speechBubbleOver);
	
		}
		
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
			
		
		private function onAgain(event:Event):void
		{
		
			game.changeState(Game.PLAY_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}