﻿package states
{ 
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import objects.BackButton;
	import objects.ButtonOne;
	import objects.ButtonTwo;
	import objects.ButtonThree;
	import objects.ButtonFour;
	import objects.ButtonFive;
	import objects.ButtonSix;
	import objects.ButtonSeven;
	import objects.ButtonEight;
	import objects.ButtonNine;
	import objects.ButtonTen;
	import objects.CountMouse;
	import objects.Wrong;
	import objects.ScoreThree;
	import interfaces.IState;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class PlayFive extends Sprite implements IState
	{
		public var game:Game;
		public var background:Background;
		public var backButton: BackButton;
		public var buttonOne: ButtonOne;
		public var buttonTwo: ButtonTwo;
		public var buttonThree: ButtonThree;
		public var buttonFour: ButtonFour;
		public var buttonFive: ButtonFive;
		public var buttonSix: ButtonSix;
		public var buttonSeven: ButtonSeven;
		public var buttonEight: ButtonEight;
		public var buttonNine: ButtonNine;
		public var buttonTen: ButtonTen;
		public var countMouse: CountMouse;
		public var countMouseArray: Array;
		public var wrong: Wrong;
		public var speechBubbleRight: SpeechBubbleRight;
		public var speechBubbleWrong: SpeechBubbleWrong;
		public var speechBubbleMouse: SpeechBubbleMouse;
		public var clock:Timer;
		public var scoreThree: ScoreThree;
		
		public function PlayFive(game:Game)
		{
			this.game = game;
		
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);
			
			scoreThree = new ScoreThree();
			addChild(scoreThree);
			scoreThree.x = 500;
			scoreThree.y = 8;
			scoreThree.width = 100;
			scoreThree.height = 75;
			
			backButton = new BackButton();
			addChild(backButton);
			backButton.x = 16;
			backButton.y = 8;
			backButton.width = 100;
			backButton.height = 100;
			backButton.addEventListener(MouseEvent.CLICK, onMenu);
			backButton.addEventListener(MouseEvent.CLICK, onClickSound);
			
			countMouseArray = new Array();
			setupCountMouse();

			buttonOne = new ButtonOne();
			addChild(buttonOne);
			buttonOne.x = 500;
			buttonOne.y = 125;
			buttonOne.width = 125;
			buttonOne.height = 100;
			buttonOne.addEventListener(MouseEvent.CLICK, onClickWrongOne); 
			buttonOne.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonOne.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonTwo = new ButtonTwo();
			addChild(buttonTwo);
			buttonTwo.x = 500;
			buttonTwo.y = 225;
			buttonTwo.width = 125;
			buttonTwo.height = 100;
			buttonTwo.addEventListener(MouseEvent.CLICK, onClickWrongTwo); 
			buttonTwo.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonTwo.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 

			buttonThree = new ButtonThree();
			addChild(buttonThree);
			buttonThree.x = 500;
			buttonThree.y = 325;
			buttonThree.width = 125;
			buttonThree.height = 100;
			buttonThree.addEventListener(MouseEvent.CLICK, onClickWrongThree); 
			buttonThree.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonThree.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonFour = new ButtonFour();
			addChild(buttonFour);
			buttonFour.x = 500;
			buttonFour.y = 425;
			buttonFour.width = 125;
			buttonFour.height = 100;
			buttonFour.addEventListener(MouseEvent.CLICK, onClickWrongFour); 
			buttonFour.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonFour.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonFive = new ButtonFive();
			addChild(buttonFive);
			buttonFive.x = 500;
			buttonFive.y = 525;
			buttonFive.width = 125;
			buttonFive.height = 100;
			buttonFive.addEventListener(MouseEvent.CLICK, onPlayFour); 
			buttonFive.addEventListener(MouseEvent.CLICK, onClickSound);
			
			buttonSix = new ButtonSix();
			addChild(buttonSix);
			buttonSix.x = 500;
			buttonSix.y = 625;
			buttonSix.width = 125;
			buttonSix.height = 100;
			buttonSix.addEventListener(MouseEvent.CLICK, onClickWrongSix); 
			buttonSix.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonSix.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonSeven = new ButtonSeven();
			addChild(buttonSeven);
			buttonSeven.x = 500;
			buttonSeven.y = 725;
			buttonSeven.width = 125;
			buttonSeven.height = 100;
			buttonSeven.addEventListener(MouseEvent.CLICK, onClickWrongSeven);
			buttonSeven.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonSeven.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonEight = new ButtonEight();
			addChild(buttonEight);
			buttonEight.x = 500;
			buttonEight.y = 825;
			buttonEight.width = 125;
			buttonEight.height = 100;
			buttonEight.addEventListener(MouseEvent.CLICK, onClickWrongEight);
			buttonEight.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonEight.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonNine = new ButtonNine();
			addChild(buttonNine);
			buttonNine.x = 500;
			buttonNine.y = 925;
			buttonNine.width = 125;
			buttonNine.height = 100;
			buttonNine.addEventListener(MouseEvent.CLICK, onClickWrongNine);
			buttonNine.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonNine.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 
			
			buttonTen = new ButtonTen();
			addChild(buttonTen);
			buttonTen.x = 500;
			buttonTen.y = 1025;
			buttonTen.width = 125;
			buttonTen.height = 100;
			buttonTen.addEventListener(MouseEvent.CLICK, onClickWrongTen);
			buttonTen.addEventListener(MouseEvent.CLICK, onClickSoundWrong);
			buttonTen.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrong); 

			speechBubbleRight = new SpeechBubbleRight();
			addChild(speechBubbleRight);
			speechBubbleRight.x = 120;
			speechBubbleRight.y = 680;
			speechBubbleRight.width = 175;
			speechBubbleRight.height = 125;
			speechBubbleRight.addEventListener(MouseEvent.CLICK, onSpeechBubble); 
			
			clock = new Timer(4000, 1);
			clock.addEventListener(TimerEvent.TIMER, onSpeechBubble)
			clock.start();
			
	}
	
		private function onSpeechBubbleClick(event:MouseEvent):void
		{
		
			speechBubbleMouse = new SpeechBubbleMouse();
			addChild(speechBubbleMouse);
			speechBubbleMouse.x = 380;
			speechBubbleMouse.y = 340;
			speechBubbleMouse.width = 175;
			speechBubbleMouse.height = 125;
			speechBubbleMouse.addEventListener(MouseEvent.CLICK, onSpeechBubbleClick); 
			
			clock = new Timer(4000, 1);
			clock.addEventListener(TimerEvent.TIMER, onSpeechBubbleClick)
			clock.start();
		}

		private function onSpeechBubble(event:Event):void
		{
		
		clock.stop();
		removeChild(speechBubbleRight);
	
		}
	
		private function onSpeechBubbleWrong(event:MouseEvent):void
		{
		
			speechBubbleWrong = new SpeechBubbleWrong();
			addChild(speechBubbleWrong);
			speechBubbleWrong.x = 120;
			speechBubbleWrong.y = 680;
			speechBubbleWrong.width = 175;
			speechBubbleWrong.height = 125;
			speechBubbleWrong.addEventListener(MouseEvent.CLICK, onSpeechBubbleWrongClick); 
			
			clock = new Timer(4000, 1);
			clock.addEventListener(TimerEvent.TIMER, onSpeechBubbleWrongClick)
			clock.start();
		}
		
		private function onSpeechBubbleWrongClick(event:MouseEvent):void
		{
		
			clock.stop();
			removeChild(speechBubbleWrong);
	
		}
	
		private function onClickSound(event:MouseEvent):void
		{
			
		var clickonSound:ClickOn = new ClickOn();
			
		var clickonSoundChannel:SoundChannel = clickonSound.play()
			
		}
		
		private function onClickSoundWrong(event:MouseEvent):void
		{
			
		var clickoffSound:ClickOff = new ClickOff();
			
		var clickoffSoundChannel:SoundChannel = clickoffSound.play()
			
		}
		
		private function onClickMouse(event:MouseEvent):void
		{
			
		var effectMouseSound:EffectMouse = new EffectMouse();
			
		var effectMouseSoundChannel:SoundChannel = effectMouseSound.play()
			
		}
		
		private function setupCountMouse() {
			
			countMouseArray = new Array();

			for (var i: int = 0; i < 5; i++) {
				for (var j: int = 0; j < 1; j++) {
					var countMouseTemp: CountMouse = new CountMouse();
					countMouseTemp.x = 175+75*i;
					countMouseTemp.y = 610+160*j;
					countMouseTemp.width = 110;
					countMouseTemp.height = 155;
					countMouseArray.push(countMouseTemp);
					this.addChild(countMouseTemp);
					countMouseTemp.addEventListener(MouseEvent.CLICK, onClickMouse); 
					countMouseTemp.addEventListener(MouseEvent.CLICK, onSpeechBubbleClick); 
				}

			}
 
		}
		
		public function onClickWrongOne(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 125;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongTwo(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 225;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongThree(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 325;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongFour(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 425;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongSix(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 625;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongSeven(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 725;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongEight(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 825;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongNine(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 925;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		
		public function onClickWrongTen(e:MouseEvent):void
		{
			wrong = new Wrong();
			addChild(wrong);
			wrong.x = 500;
			wrong.y = 1025;
			wrong.width = 125;
			wrong.height = 100;
			
		}
		 
		private function onPlayFour(event:Event):void
		{
			game.changeState(Game.PLAYFOUR_STATE);
		}
		
		private function onGameOver(event:Event):void
		{
			game.changeState(Game.GAME_OVER_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			background.update();
			
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}