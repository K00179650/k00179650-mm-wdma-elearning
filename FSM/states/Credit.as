﻿package states
{ 
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import objects.Background;
	import objects.Title;
	import objects.BackButton;
	import objects.CreditTitle;
	import objects.EMailButton;
	import objects.LinkedInButton;
	import objects.YouTubeButton;
	import interfaces.IState;
	import flash.media.SoundChannel;
	
	public class Credit extends Sprite implements IState
	{
		public var game:Game;
		public var background:Background;
		public var backButton: BackButton;
		public var creditTitle: CreditTitle;
		public var eMailButton: EMailButton;
		public var linkedInButton: LinkedInButton;
		public var youTubeButton: YouTubeButton;

		public function Credit(game:Game)
		{
			this.game = game;
			
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);

			backButton = new BackButton();
			addChild(backButton);
			backButton.x = 34;
			backButton.y = 16;
			backButton.width = 100;
			backButton.height = 100;
			backButton.addEventListener(MouseEvent.CLICK, onMenu); 
			backButton.addEventListener(MouseEvent.CLICK, onClickSound);

			creditTitle = new CreditTitle();
			addChild(creditTitle);
			creditTitle.x = 175;
			creditTitle.y = 68;
			creditTitle.width = 300;
			creditTitle.height = 150;
			
			eMailButton = new EMailButton();
			addChild(eMailButton);
			eMailButton.x = 245;
			eMailButton.y = 325;
			eMailButton.width = 175;
			eMailButton.height = 125;
			eMailButton.addEventListener(MouseEvent.CLICK, onClickEMail);
			eMailButton.addEventListener(MouseEvent.CLICK, onClickSound);
			
			linkedInButton = new LinkedInButton();
			addChild(linkedInButton);
			linkedInButton.x = 245;
			linkedInButton.y = 525;
			linkedInButton.width = 175;
			linkedInButton.height = 125;
			linkedInButton.addEventListener(MouseEvent.CLICK, onClickLinkedIn);
			linkedInButton.addEventListener(MouseEvent.CLICK, onClickSound);

			youTubeButton = new YouTubeButton();
			addChild(youTubeButton);
			youTubeButton.x = 245;
			youTubeButton.y = 725;
			youTubeButton.width = 175;
			youTubeButton.height = 125;
			youTubeButton.addEventListener(MouseEvent.CLICK, onClickYouTube);
			youTubeButton.addEventListener(MouseEvent.CLICK, onClickSound);
			
		}

		private function onClickSound(event:MouseEvent):void
		{
			
		var clickonSound:ClickOn = new ClickOn();
			
		var clickonSoundChannel:SoundChannel = clickonSound.play()
			
		}
		
 		public function onClickEMail(event:MouseEvent):void
		{	
			var linkEmail:URLRequest = new URLRequest("mailto:Shaneonash93@hotmail.com?subject=Subject&body=Message");
			navigateToURL(linkEmail, "_blank");
		}

		 public function onClickLinkedIn(event:MouseEvent):void
		{	
			var linkLinkedIn:URLRequest = new URLRequest("http://ie.linkedin.com/pub/shane-nash/b1/65b/b01");
			navigateToURL(linkLinkedIn, "_blank");
		}

		 public function onClickYouTube(event:MouseEvent):void
		{	
			var linkYouTube:URLRequest = new URLRequest("https://www.youtube.com/channel/UCqb-CmXM4W548l2QsmymuSw");
			navigateToURL(linkYouTube, "_blank");
		}

		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}