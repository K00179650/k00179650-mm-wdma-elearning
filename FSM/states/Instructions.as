﻿package states
{ 
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import objects.Background;
	import objects.Title;
	import objects.BackButton;
	import objects.InstructionTitle;
	import objects.InstructionText;
	import objects.CountMouse;
	import objects.BeginButton;
	import objects.ButtonFive;
	import interfaces.IState;
	import flash.media.SoundChannel;
	
	public class Instructions extends Sprite implements IState
	{
		public var game:Game;
		public var background:Background;
		public var backButton: BackButton;
		public var instructionTitle: InstructionTitle;
		public var instructionText: InstructionText;
		public var countMouse: CountMouse;
		public var beginButtonImage: BeginButton;
		public var buttonFiveImage: ButtonFive;
		
		public function Instructions(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);
						
			backButton = new BackButton();
			addChild(backButton);
			backButton.x = 34;
			backButton.y = 16;
			backButton.width = 100;
			backButton.height = 100;
			backButton.addEventListener(MouseEvent.CLICK, onMenu); 
			backButton.addEventListener(MouseEvent.CLICK, onClickSound); 
			
			instructionTitle = new InstructionTitle();
			addChild(instructionTitle);
			instructionTitle.x = 175;
			instructionTitle.y = 68;
			instructionTitle.width = 300;
			instructionTitle.height = 150;
			
			instructionText = new InstructionText();
			addChild(instructionText);
			instructionText.x = 34;
			instructionText.y = 237;
			instructionText.width = 575;
			instructionText.height = 525;
			
			countMouse = new CountMouse();
			addChild(countMouse);
			countMouse.x = 180;
			countMouse.y = 630;
			countMouse.width = 141;
			countMouse.height = 207;
			countMouse.addEventListener(MouseEvent.CLICK, onClickMouse); 

			beginButtonImage = new BeginButton();
			addChild(beginButtonImage);
			beginButtonImage.x = 385;
			beginButtonImage.y = 350;
			beginButtonImage.width = 175;
			beginButtonImage.height = 125;
			beginButtonImage.addEventListener(MouseEvent.CLICK, onPlay); 
			beginButtonImage.addEventListener(MouseEvent.CLICK, onClickSound); 

			buttonFiveImage = new ButtonFive();
			addChild(buttonFiveImage);
			buttonFiveImage.x = 385;
			buttonFiveImage.y = 550;
			buttonFiveImage.width = 175;
			buttonFiveImage.height = 125;
			buttonFiveImage.addEventListener(MouseEvent.CLICK, onClickSoundWrong); 
		}

		private function onClickSound(event:MouseEvent):void
		{
			
		var clickonSound:ClickOn = new ClickOn();
			
		var clickonSoundChannel:SoundChannel = clickonSound.play()
			
		}
	 
		private function onClickSoundWrong(event:MouseEvent):void
		{
			
		var clickoffSound:ClickOff = new ClickOff();
			
		var clickoffSoundChannel:SoundChannel = clickoffSound.play()
			
		}
			
		private function onClickMouse(event:MouseEvent):void
		{
			
		var effectMouseSound:EffectMouse = new EffectMouse();
			
		var effectMouseSoundChannel:SoundChannel = effectMouseSound.play()
			
		}
		
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}