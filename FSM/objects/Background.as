﻿package objects
{
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	 
	public class Background extends Sprite
	{
		public var enviroment:Enviroment;
		public var character:Character;
		public var speechBubbleClick: SpeechBubbleClick;
		public var clock:Timer;
		var christmasEveAtMidnightSmallTownSquareSound:ChristmasEveAtMidnightSmallTownSquare = new ChristmasEveAtMidnightSmallTownSquare();
		var christmasEveAtMidnightSmallTownSquareSoundChannel:SoundChannel = christmasEveAtMidnightSmallTownSquareSound.play()

		public function Background()
		{
			enviroment = new Enviroment();
			addChild(enviroment);
			enviroment.x = 640;
			enviroment.y = 1136;
			enviroment.width = 640;
			enviroment.height = 1136;
			
			character = new Character();
			addChild(character);
			character.x = 15;
			character.y = 775;
			character.width = 175;
			character.height = 250;
			character.addEventListener(MouseEvent.CLICK, onClickOwl);
			character.addEventListener(MouseEvent.CLICK, onSpeechBubble);
			
		}
		
		private function onSpeechBubble(event:MouseEvent):void
		{
		
			speechBubbleClick = new SpeechBubbleClick();
			addChild(speechBubbleClick);
			speechBubbleClick.x = 120;
			speechBubbleClick.y = 680;
			speechBubbleClick.width = 175;
			speechBubbleClick.height = 125;
			speechBubbleClick.addEventListener(MouseEvent.CLICK, onSpeechBubbleClick); 
			
			clock = new Timer(4000, 1);
			clock.addEventListener(TimerEvent.TIMER, onSpeechBubbleClick)
			clock.start();
		}
		
		private function onSpeechBubbleClick(event:MouseEvent):void
		{
		
			clock.stop();
			removeChild(speechBubbleClick);
	
		}
		
		private function onClickOwl(event:MouseEvent):void
		{
			
		var owlSound:Owl = new Owl();
			
		var owlSoundChannel:SoundChannel = owlSound.play()
			
		}
		public function update():void
		{

		}
		
		public function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}