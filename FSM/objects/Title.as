﻿package objects
{ 
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.AntiAliasType;
	import flash.text.Font;

	public class Title extends Sprite
	{
		
		public var titleText:TextField;
		public function Title()
		
		{
			
			var djbChalkItUp = new DJBChalkItUp();
			
			var myFormat:TextFormat = new TextFormat();
			
			myFormat.bold = true; 
			myFormat.color = 0xFFFFFF;     
			myFormat.size = 53;
			myFormat.font = djbChalkItUp.fontName;

			titleText = new TextField();
			titleText.defaultTextFormat = myFormat;
			addChild(titleText);
			titleText.x=245;
			titleText.y=225;
			titleText.width = 175;
			titleText.height = 125;
			titleText.embedFonts = true;
			titleText.antiAliasType = AntiAliasType.ADVANCED;
			
		}
		
		public function setTitle(title:String):void
		{
			titleText.text = title;
		}
	}
}