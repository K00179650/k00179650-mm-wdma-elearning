﻿package 
{
	import interfaces.IState;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import states.Menu;
	import states.Play;
	import states.Instructions;
	import states.GameOver;
	import states.Credit;
	import states.PlayOne;
	import states.PlayTwo;
	import states.PlayThree;
	import states.PlayFour;
	import states.PlayFive;
	import states.PlaySix;
	import states.PlaySeven;
	import states.PlayEight;
	import states.PlayNine;
	import flash.display.StageScaleMode;
	
	public class Game extends Sprite
	{
		public static const MENU_STATE:int = 0;
		public static const PLAY_STATE:int = 1;
		public static const INSTRUCTIONS_STATE:int = 2;
		public static const GAME_OVER_STATE:int = 3;
		public static const CREDIT_STATE:int = 4;
		public static const PLAYONE_STATE:int = 5;
		public static const PLAYTWO_STATE:int = 6;
		public static const PLAYTHREE_STATE:int = 7;
		public static const PLAYFOUR_STATE:int = 8;
		public static const PLAYFIVE_STATE:int = 9;
		public static const PLAYSIX_STATE:int = 10;
		public static const PLAYSEVEN_STATE:int = 11;
		public static const PLAYEIGHT_STATE:int = 12;
		public static const PLAYNINE_STATE:int = 13;
		
		private var current_state:IState;
		
		public function Game()
		{
	
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			changeState(MENU_STATE);
			addEventListener(Event.ENTER_FRAME, update);
			stage.scaleMode = StageScaleMode.EXACT_FIT;
		}
		
		public function changeState(state:int):void
		{
			if(current_state != null)
			{
				current_state.destroy();
				current_state = null;
			}
			
			switch(state)
			{
				case MENU_STATE:
					current_state = new Menu(this);
					break;
				
				case PLAY_STATE:
					current_state = new Play(this);
					break;
				
				case INSTRUCTIONS_STATE:
					current_state = new Instructions(this);
					break;
				
				case GAME_OVER_STATE:
					current_state = new GameOver(this);
					break;
					
				case CREDIT_STATE:
					current_state = new Credit(this);
					break;
					
				case PLAYONE_STATE:
					current_state = new PlayOne(this);
					break;
					
				case PLAYTWO_STATE:
					current_state = new PlayTwo(this);
					break;
					
				case PLAYTHREE_STATE:
					current_state = new PlayThree(this);
					break;	
					
				case PLAYFOUR_STATE:
					current_state = new PlayFour(this);
					break;
					
				case PLAYFIVE_STATE:
					current_state = new PlayFive(this);
					break;	
					
				case PLAYSIX_STATE:
					current_state = new PlaySix(this);
					break;	
					
				case PLAYSEVEN_STATE:
					current_state = new PlaySeven(this);
					break;	
				
				case PLAYEIGHT_STATE:
					current_state = new PlayEight(this);
					break;
					
				case PLAYNINE_STATE:
					current_state = new PlayNine(this);
					break;	
			}
			
			addChild(Sprite(current_state));
		}
		
		private function update(event:Event):void
		{
			current_state.update();
		}
	}
}